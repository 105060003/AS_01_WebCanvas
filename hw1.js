// Canvas DOM 元素 
const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

//起始點座標
let x1= 0;
let y1= 0;

// 終點座標
let x2= 0;
let y2= 0;

//rectangle起點
let x = 0;
let y = 0;

ctx.lineWidth = 1;// 線條寬度
ctx.lineCap = 'round'; // 線條兩端圓弧
ctx.lineJoin = 'round';  // 線條折角圓弧

var tool = "brush";

// 透過上方的 hasTouchEvent 來決定要監聽的是 mouse 還是 touch 的事件
const downEvent = 'mousedown';
const moveEvent = 'mousemove';
const upEvent = 'mouseup';

// 宣告 isMouseActive 為滑鼠點擊的狀態，因為我們需要滑鼠在 mousedown 的狀態時，才會監聽 mousemove 的狀態
let isMouseActive = false;
let changingColor = 0; //color variable responsible for changing color
var cPushArray = new Array(); 
var cStep = -1;

//initialize
(function () {
  ctx.fillStyle = "rgb(255,255,255)";
  ctx.fillRect(0,0,canvas.width,canvas.height);
  cPush();
})();

function cPush() {
  console.log("push");
  cStep++;
  if (cStep < cPushArray.length) { cPushArray.length = cStep; }
  cPushArray.push(document.getElementById('canvas').toDataURL());
}

canvas.addEventListener(downEvent, function(e){
  isMouseActive = true  ;
  x1 = e.offsetX;
  y1 = e.offsetY;
  x = e.offsetX;
  y = e.offsetY; 
});

canvas.addEventListener(moveEvent, function(e){
      if(!isMouseActive){
        return
      }
      // 取得終點座標
      x2 = e.offsetX;
      y2 = e.offsetY;
      
      // 開始繪圖
      ctx.beginPath();
      ctx.moveTo(x1, y1);
      ctx.lineTo(x2, y2);

      if(tool == "brush" || tool == "eraser")
        ctx.stroke();
      if(tool == "rainbow")
      {
        ctx.strokeStyle = `hsl(${changingColor}, 100%, 50%)`;
        ctx.stroke();
        changingColor++;
        //reset hue to 0 if it goes past 360
        if (changingColor >= 360) changingColor = 0;
      }
      // 更新起始點座標
      x1 = x2;
      y1 = y2; 
});

function rgbToHex(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
  }

  
function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
  }
    
canvas.addEventListener(upEvent, function(e){
  isMouseActive = false;
  if(tool == "select_color")
  {
    var temp = ctx.getImageData(x, y, 1, 1).data;
    var hex = rgbToHex (temp[0],temp[1],temp[2]);
    console.log(hex);
    document.getElementById("color").value = hex;
    ctx.fillStyle = document.getElementById("color").value;
    ctx.strokeStyle = document.getElementById("color").value;
  }
  if(tool == "star")
  {
    ctx.save();
    ctx.beginPath();
    ctx.translate(x, y);
    ctx.moveTo(0,0-r);
    var r = Math.sqrt(((x2-x)*(x2-x) + (y2-y)*(y2-y))/2) ;
    p = 5;
    for (var i = 0; i < p; i++)
    {
        ctx.rotate(Math.PI / p);
        ctx.lineTo(0, 0 - (r*0.5));
        ctx.rotate(Math.PI / p);
        ctx.lineTo(0, 0 - r);
    }
    ctx.fill();
    ctx.restore();
  }
  if(tool != "star" && tool != "line"){
    var circle = new Path2D();
    circle.moveTo(x, y);
    circle.arc((x+x2)/2, (y+y2)/2, Math.sqrt(((x2-x)*(x2-x) + (y2-y)*(y2-y))/2) , 0, 2 * Math.PI);
    ctx.beginPath();
    ctx.moveTo(x,y);
    ctx.lineTo(x2,y2);
    ctx.lineTo(2*x-x2,y2);
  }
  if(tool == "line")
  {
    ctx.beginPath();
    ctx.moveTo(x,y);
    ctx.lineTo(x2,y2);
    ctx.stroke();
  }
  if(tool == "rectangle"){
    ctx.fillRect(x,y,x2-x,y2-y);
  }
  else if(tool == "circle")
  {
    ctx.fill(circle);
  }
  else if(tool == "triangle")
  {
    ctx.fill();
  }
  else if(tool == "text")
  {
    if(message == "")
      alert("Please type something first");
    else
      ctx.fillText  (message, x1, y1);
  }
  else if(tool == "image")
  {
      if(img_upload == -1)
        alert('Please upload picture first \nOR be sure to upload "picture"');
      else 
        ctx.drawImage(img, x,y,img.width,img.height);
  }
  else 
  {
    document.getElementById("text").disabled = true;
    document.getElementById("text").value = "";
    message = "";
  }
  
   cPush();
})

//change brush size
document.getElementById("brushsize").addEventListener('change', function() {
    ctx.lineWidth = this.value;
    document.getElementById("brushsize").innerHTML = this.value;
}, false);

//change color
document.getElementById("color").addEventListener('change', function() {
  ctx.strokeStyle = this.value;
  ctx.fillStyle = this.value;
}, false);

//eraser
document.getElementById("eraser").addEventListener('click', function() {
  tool = "eraser";
  ctx.strokeStyle = "rgb(255, 255, 255)";
  document.getElementById("canvas").style.cursor = "url('eraser.png'), auto";
}, false);

//rainbow
document.getElementById("rainbow").addEventListener('click', function() {
  tool = "rainbow";
  document.getElementById("canvas").style.cursor = "url('rainbow_pen.png'), auto";
}, false);

//brush
document.getElementById("brush").addEventListener('click', function() {
  tool = "brush";
  ctx.strokeStyle = document.getElementById("color").value;
  document.getElementById("canvas").style.cursor = "url('pencil.png'), auto";
}, false);

//text input
ctx.font = "30px Arial";
var message = ""; 
document.getElementById("text").addEventListener('change', function() {
  message = this.value;
  //console.log(this.value);
}, false);

document.getElementById("text_button").addEventListener('click', function() {
  tool = "text";
  document.getElementById("text").disabled = false;
  document.getElementById("canvas").style.cursor = "text";
  ctx.fillStyle = document.getElementById("color").value;
}, false);
var fontsize = "30px";
var fonttype = "Arial";
document.getElementById("typeface").addEventListener('change', function() {
  ctx.font = fontsize + " " + this.value;
  fonttype = this.value;
  console.log(this.value);
}, false);
document.getElementById("fontsize").addEventListener('change', function() {
  ctx.font = this.value + " " +fonttype;
  fontsize = this.value;
  console.log(this.value);
}, false);

//reset canvas
document.getElementById("reset").addEventListener('click', function() {
  ctx.fillStyle = "rgb(255,255,255)";
  ctx.fillRect(0,0,canvas.width,canvas.height);
  cPush();
  //console.log("reset");
}, false);

//draw rectangle
var rectangle_button = -1;
document.getElementById("rectangle").addEventListener('click', function() {
  tool = "rectangle";
  ctx.strokeStyle = document.getElementById("color").value;
  ctx.fillStyle = document.getElementById("color").value;
  document.getElementById("canvas").style.cursor = "url('rectangle.png'), auto";
}, false);


//draw triangle
document.getElementById("triangle").addEventListener('click', function() {
  tool = "triangle";
  ctx.strokeStyle = document.getElementById("color").value;
  ctx.fillStyle = document.getElementById("color").value;
  document.getElementById("canvas").style.cursor = "url('triangle.png'), auto";
}, false);

//draw circle
document.getElementById("circle").addEventListener('click', function() {
  console.log("circle");
  tool = "circle";
  ctx.strokeStyle = document.getElementById("color").value;
  ctx.fillStyle = document.getElementById("color").value;
  document.getElementById("canvas").style.cursor = "url('circle.png'), auto";
}, false);


//download canvas image
function download() {
  console.log("download button");
  var download = document.getElementById("download");
  var image = document.getElementById("canvas").toDataURL("image/jpeg")
  download.setAttribute("href", image);
  }



var img = new Image();
var img_upload = -1;
//upload image 
document.getElementById('upload').addEventListener("change",function(e){
  img_upload = 1;
  img.src = URL.createObjectURL(e.target.files[0]);
});

document.getElementById('paste_image').addEventListener("click",function(e){
  tool = "image";
  document.getElementById("canvas").style.cursor = "url('image.png'), auto";
});

document.getElementById("star").addEventListener('click', function() {
  tool = "star";
  ctx.fillStyle = document.getElementById("color").value;
  ctx.strokeStyle = document.getElementById("color").value;
  document.getElementById("canvas").style.cursor = "url('star_cur.png'), auto";
}, false);

document.getElementById("line").addEventListener('click', function() {
  tool = "line";
  ctx.strokeStyle = document.getElementById("color").value;
  ctx.fillStyle = document.getElementById("color").value;
  document.getElementById("canvas").style.cursor = "crosshair";
}, false);

document.getElementById("undo").addEventListener('click', function() {
  if (cStep > 0) {
    ctx.fillStyle = "rgb(255,255,255)";
    ctx.fillRect(0,0,canvas.width,canvas.height);
    cStep--;
    var canvasPic = new Image();
    canvasPic.src = cPushArray[cStep];
    canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
}
}, false);

document.getElementById("redo").addEventListener('click', function() {
  if (cStep < cPushArray.length-1) {
    ctx.fillStyle = "rgb(255,255,255)";
    ctx.fillRect(0,0,canvas.width,canvas.height);
    cStep++;
    var canvasPic = new Image();
    canvasPic.src = cPushArray[cStep];
    canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
}
}, false);


document.getElementById("image_color").addEventListener('click', function() {
  tool = "select_color";
  document.getElementById("canvas").style.cursor = "url('dropper.png'), auto";
}, false);



//偵測是否有按ctrl v
['cut', 'copy', 'paste'].forEach(function(event) {
	 
    document.addEventListener(event, function(e) {
        console.log("event: " + event);   
        if(event == "paste"){
          if(e.clipboardData == false) return false; 
          var imgs = e.clipboardData.items;
          if(imgs == undefined) return false;
          for (var i = 0; i < imgs.length; i++) {
          if (imgs[i].type.indexOf("image") == -1) continue;
          var imgObj = imgs[i].getAsFile();
          var url = window.URL || window.webkitURL;
          var src = url.createObjectURL(imgObj);
          //ctx.clearRect(0,0,canvas.width,canvas.height);
          loadImage(src);
          
        }
      }
    });
});

function loadImage(src){
  var img = new Image();
  img.onload = function(e) {
    ctx.drawImage(img,0,0);
    cPush();
  };
  img.src = src;
}

         
