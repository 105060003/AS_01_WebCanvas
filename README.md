# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
### 網頁整體介紹
* 中間是畫布
* 畫布左邊和右邊和下面是各式各樣的工具

### 畫布工具功能操作介紹
#### Basic Components
1. Color
    * 顏色選擇器
    * 點下去後即可選擇自己想要的顏色
2. Brush Size
    * 調整brush,eraser,line大小粗細
    * 透過拉拉桿來調整，由左到右是小(細)到大(粗)
3. Brush
    * 畫布主要的筆刷
    * 點下按鈕即可使用
    * 可透過調整brush size,color等等調整筆刷大小,顏色
4. Eraser
    * 畫布橡皮擦
    * 每次按鈕按下預設皆為白色，可透過顏色選擇器選擇顏色
    * 可透過brush size調整橡皮擦大小
5. Text Input
    * 先點擊畫布下方的"T"按鈕
    * 然後在右方的"Type something here"打字
    * 支援中、英文等等
    * 最後在畫布上點擊想要的位置即可貼上字
    * 如果沒有打字就直接點擊畫布的話，會跳出alert視窗提醒使用者先打字
6. Text Size
    * 在text input右邊，可選擇字體大小
7. Text Type
    * 在text size右邊，可選擇不同字型
8. Cursor
    * 我在Eraser,Brush,Rectangle,Circle,Triangle,Rainbow,Star,Line,Text Input,Paste Image,Pick Color Directly皆有設定不同的cursor
    * 點擊工具按鈕後，將滑鼠移到canvas上即可看見
9. Reset
    * 點擊按鈕即可立即清除畫布，回到預設白畫布狀態

#### Advanced tools

10. Different brush shapes: Circle,Triangle,Rectangle
    * 點擊按鈕後，即可在畫布上用滑鼠拉出適合的大小和圖形
    * 可透過顏色選擇器調整顏色
11. Un/Re-do button: Undo,Redo
    * 點擊按鈕和即可undo,redo畫布
12. Image tool: 
    * 先在text type右邊的"選擇檔案"上傳想要的圖檔
    * 然後點擊畫布左邊的"Paste Image"
    * 最後在畫布上想要的位置點擊，即可貼上去
    * 如果尚未上傳圖檔就想貼圖片到畫布的話，會跳出alert提醒使用者應先上傳圖檔
13. Download
    * 點擊按鈕即可下載整張畫布，下載格式為.jpg

#### Other userful widgets
14. Rainbow
    * 彩虹畫筆
    * 點擊按鈕後即可使用，跟brush使用方法相同
15. Star
    * 點擊按鈕後，在畫布上找適合的位置拉出星星圖形即可
    * 跟circle,rectangle,triangle使用方法相同
16. Line
    * 點擊按鈕後，即可在畫布上拉出直線
    * 也可透過顏色選擇器,brush size調整顏色,大小
17. Pick color directly
    * 使用方法:點擊此按鈕後,直接點選畫布上中自己想要的顏色位置,顏色選擇器即會變為此顏色
    * 讓使用者不用一直挑(調)顏色，輕鬆選擇上傳到畫布的圖片中的顏色，或是挑選先前畫過的顏色
18. Paste image from clipboard directly
    * 可以直接將在外部複製且在剪貼簿中的圖片,按Ctrl V後即可直接貼在自己的畫布上

### 畫布以及功能實作方法
#### CSS及排版
* 排版皆是自己排的
* 按鈕中的icon圖片來源皆為"https://www.flaticon.com/"

#### Javascript
1. 透過canvas.addEventListener監聽mouse down,mouse move,mouse up事件進而在畫布上操作
2. 每個按鈕都是用document.getElementById("xxxxxxxx").addEventListener來監聽是否按下，進而切換工具功能
3. 按下按鈕後會改變var tool像是tool = "brush,tool = "eraser"等等來決定現在是在使用什麼tool
4. 在畫布上畫畫幾乎都是透過API實作而成，以下列舉我有用到的API:<br>
* const canvas = document.getElementById('canvas');<br>
* const ctx = canvas.getContext('2d');<br>
    * ctx.beginPath(); 
    * ctx.moveTo(x1, y1);
    * ctx.lineTo(x2, y2);
    * ctx.rotate(Math.PI / p);
    * ctx.stroke();
    * ctx.strokeStyle = document.getElementById("color").value;
    * ctx.translate(x, y);
    * ctx.fillStyle = "rgb(255,255,255)";
    * ctx.fillRect(0,0,canvas.width,canvas.height);
    * ctx.fill();
    * ctx.restore();
    * ctx.drawImage(img, x,y,img.width,img.height);
    * ctx.fillText  (message, x1, y1);
    * ctx.lineWidth = 1;
    * ctx.lineCap = 'round'; 
    * ctx.lineJoin = 'round'; 
5. Undo/Redo的實作是寫cPush() function，以及在Undo,Redo按鈕下寫function code，主要實作原理是:<br>每做完一件會改變canvas內容的事時，就呼叫cPush()，它會PUSH現在canvas的data url進array，之後Undo/Redo都是從array裡拿canvas data url進而重畫到畫布上
